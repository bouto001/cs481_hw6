﻿using OnlineDictionary.Objects;
using OnlineDictionary.Settings;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OnlineDictionary.Services
{
    // API Service : where we going to store all calls that can hit the dictionary api
    public class DictionaryService
    {
        private readonly HttpClient Client;
        private static readonly DictionaryApi DictionaryApi = new DictionaryApi();

        public DictionaryService()
        {
            Client = new HttpClient();
            
            // Insert here the token given by the api provider
            Client.DefaultRequestHeaders.Add("Authorization", string.Format("Token {0}", DictionaryApi.Token));
        }

        public async Task<SearchResult> GetDefinitionAsync(string word)
        {
            HttpResponseMessage response = await Client.GetAsync(string.Format("{0}/{1}", DictionaryApi.ApiBaseUrl, word));

            SearchResult searchResult = null;
            if (response.IsSuccessStatusCode)
            {
                // Json string to C# object
                var rawResult = await response.Content.ReadAsStringAsync();
                searchResult = JsonConvert.DeserializeObject<SearchResult>(rawResult);
            }

            //If searchResult still null, it means that the request fails.
            return searchResult;
        }
    }
}
