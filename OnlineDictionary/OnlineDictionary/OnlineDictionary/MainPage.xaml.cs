﻿using OnlineDictionary.Objects;
using OnlineDictionary.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace OnlineDictionary
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public SearchResult Result { get; set; } = new SearchResult();
        private readonly DictionaryService DictionaryService;
        public MainPage()
        {
            InitializeComponent();

            BindingContext = Result;
            DictionaryService = new DictionaryService();

            //Initialize graphical component states
            Loader.IsRunning = false;
            WordDefView.IsVisible = false;

            OnPropertyChanged(nameof(Result));
        }

        //Implementation of the Dictionary Service

        public async Task PerformSearch(string word)
        {
            Loader.IsVisible = true;
            WordDefView.IsVisible = false;

            // Check if the device is connected to the internet
            if (!(await IsInternetAvailable()))
                return;

            Result = await DictionaryService.GetDefinitionAsync(word);

            // If The request failed
            if (Result == null)
            {
                await DisplayAlert("Oops", $"No definition found for {UserEntry.Text}", "Ok");
                return;
            }

            WordDefinitions.ItemsSource = Result.Definitions;
            OnPropertyChanged(nameof(Result));

            Loader.IsVisible = false;
            WordDefView.IsVisible = true;

            if (Result.Definitions.Count == 0)
            {
                await DisplayAlert("Oops", $"No definition found for {UserEntry.Text}", "Ok");
            }
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await PerformSearch(UserEntry.Text);
        }

        public async Task<bool> IsInternetAvailable()
        {
            var ret = NetworkInterface.GetIsNetworkAvailable();

            if (!ret)
                await DisplayAlert("Oops", "Not connected to the internet, please check your connection", "got it");

            return ret;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            // Check if the device is connected to the internet
            await IsInternetAvailable();

        }
    }
}
