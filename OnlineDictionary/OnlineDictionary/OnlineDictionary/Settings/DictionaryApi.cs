﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineDictionary.Settings
{
    // Hard coding static data such as Token or Api url
    public class DictionaryApi
    {
        public string Token { get; set; } = "604f8cde3ff089917758964c366160a1d694e1c5";
        public string ApiBaseUrl { get; set; } = " https://owlbot.info/api/v4/dictionary";
    }
}
