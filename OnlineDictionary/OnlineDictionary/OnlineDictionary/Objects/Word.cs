﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace OnlineDictionary.Objects
{
    // Object that represents the return object of the Dictionary API
    public class Word
    {
        // Assign default values, just in case of empty result 
        public string Type { get; set; } = "-";
        public string Definition { get; set; } = "-";
        public string Example { get; set; } = "-";
    }

    public class SearchResult
    {
        public List<Word> Definitions { get ; set; }
        public string Word { get; set; }
        public string Pronunciation { get; set; }
        public int DefinitionCount { get; set; } = 1;
    }
}
